const Webpack = require("webpack");
const ExtractTextPlugin = require("extract-text-webpack-plugin");
const CopyPlugin = require("copy-webpack-plugin");
const path = require("path");

module.exports = {
  entry: [
    "./js/public/index.js",
    "./styles/public/index.less"
  ],
  output: {
    path: path.join(__dirname, "../priv/static"),
    filename: "js/app.js",
    publicPath: "/"
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        loader: "babel-loader",
        query: {
          plugins: ["transform-decorators-legacy"],
          presets: ["react", "es2015", "stage-2"]
        }
      },
      {
        test: /\.less$/,
        use: ExtractTextPlugin.extract({
          fallback: "style-loader",
          use: ["css-loader", "less-loader"]
        })
      }
    ]
  },
  resolve: {
    extensions: [".js", ".less", ".css"],
    modules: [
      path.join(__dirname, "js/lib"),
      path.join(__dirname, "js/public"),
      "node_modules"
    ]
  },
  plugins: [
    new ExtractTextPlugin({
      filename: "css/app.css",
      allChunks: true
    }),
    new CopyPlugin([{from: "./static"}])
  ]
};
