import React from 'react'
import PropTypes from 'prop-types';
import { Route } from 'react-router'
import { Link } from 'react-router-dom'


/**
 * A <Link> wrapper that knows if it's "active" or not.
 */
const LiNavLink = ({
  to,
  exact,
  strict,
  activeClassName,
  liActiveClassName,
  className,
  liClassName,
  activeStyle,
  liActiveStyle,
  style,
  liStyle,
  isActive: getIsActive,
  ...rest
}) => (
  <Route
    path={typeof to === 'object' ? to.pathname : to}
    exact={exact}
    strict={strict}
    children={({ location, match }) => {
      const isActive = !!(getIsActive ? getIsActive(match, location) : match)

      return (
        <li
          className={isActive ? [ liActiveClassName, liClassName ].join(' ') : liClassName}
          style={isActive ? { ...liStyle, ...liActiveStyle } : liStyle}
        >
          <Link
            to={to}
            className={isActive ? [ activeClassName, className ].join(' ') : className}
            style={isActive ? { ...style, ...activeStyle } : style}
            {...rest}
          />
        </li>
      )
    }}
  />
)
LiNavLink.propTypes = {
  to: Link.propTypes.to,
  exact: PropTypes.bool,
  strict: PropTypes.bool,
  activeClassName: PropTypes.string,
  liActiveClassName: PropTypes.string,
  className: PropTypes.string,
  liClassName: PropTypes.string,
  activeStyle: PropTypes.object,
  liActiveStyle: PropTypes.object,
  style: PropTypes.object,
  liStyle: PropTypes.object,
  isActive: PropTypes.func
}

LiNavLink.defaultProps = {
  liActiveClassName: 'active'
}

export default LiNavLink
