import { Socket } from 'phoenix';


const setupHandlers = (name, channel, dispatch) => {
  switch (name) {
    case "jobs":
      channel.on("init", (msg) => {
        dispatch({
          type: "JOBS_INIT",
          jobs: msg.jobs
        });
      });
      channel.on("new", (msg) => {
        dispatch({
          type: "JOBS_NEW",
          jobs: msg.jobs
        });
      });
      break;
    case "user":
      channel.on("jobs_reset", (msg) => {
        dispatch({
          type: 'JOBS_INIT',
          jobs: msg.jobs
        });
      });
      break;
    default:
      break;
  }
}

export default {
  // This function will disconnect first if connection was established before
  socket_connect: () => {
    return (dispatch, getState) => {
      const { ws } = getState();
      let socket = ws.socket;
      if (socket !== null) {
        socket.disconnect();
      }
      const params = {token: localStorage.getItem('phoenixAuthToken')};
      const logger = (kind, msg, data) => { console.log(`${kind}: ${msg}`, data); };
      socket = new Socket('/socket', {params, logger});
      socket.connect();
      dispatch({
        type: 'SOCKET_CONNECTED',
        socket: socket
      });
    }
  },
  // This function will join given channel and then setup it's handlers based on setupHandlers function
  channel_join: (name, alias) => {
    return (dispatch, getState) => {
      const { ws } = getState();
      const channel = ws.socket.channel(name);
      channel.join().receive('ok', () => {
        setupHandlers(alias, channel, dispatch);
      });
      dispatch({
        type: 'CHANNEL_JOINED',
        name: alias,
        channel: channel
      });
    }
  }
};
