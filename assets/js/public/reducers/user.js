const initialState = {
  authorized: false,
  email: null,
  filters: [],
  notifications: []
};

export default function reducer(state = initialState, action) {
  switch (action.type) {
    case "LOGIN":
      return { ...state, authorized: true };
      break;
    case "LOGOUT":
      return { ...state, authorized: false };
      break;
    case "SET_INFO":
      return { ...state, email: action.email, filters: action.filters, notifications: action.notifications }
    default:
      return state
  }
};
