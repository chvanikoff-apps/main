import React from "react";

export default class Home extends React.Component {
  render() {
    return <div className="projects">
      <ul>
        <li>
          <a href="/todo">ToDo app</a>
        </li>
        <li>
          <a href="/filesharing">File hosting app</a>
        </li>
        <li>
          <a href="/imagesharing">Image hosting app</a>
        </li>
      </ul>
    </div>;
  }
}
