import React from "react";
import { Link } from "react-router-dom";

import LiNavLink from "LiNavLink";


export default class Navigation extends React.Component {
  render() {
    return <nav className="navbar navbar-default navbar-fixed-top">
      <div className="container">
        <div className="navbar-header">
          <a className="navbar-brand" href="https://flfeed.com">FLFeed.com</a>
        </div>
        <div  className="navbar-collapse collapse">
          <ul className="nav navbar-nav">
            <LiNavLink exact to="/">Home</LiNavLink>
            <LiNavLink to="/about">About</LiNavLink>
          </ul>
        </div>
      </div>
    </nav>;
  }
}
