import React from "react";
import ReactDOM from "react-dom";
import { createStore } from "redux";
import { Provider } from "react-redux";

import reducers from "reducers";
import Index from "containers/index.js";

const store = createStore(reducers);
const index = <Provider store={store}>
  <Index />
</Provider>;


ReactDOM.render(index, document.getElementById("index"));
