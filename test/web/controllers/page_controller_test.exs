defmodule Main.Web.PageControllerTest do
  use Main.Web.ConnCase

  test "Link to compiled JS is present in HTML code", %{conn: conn} do
    conn = get conn, "/"
    assert html_response(conn, 200) =~ "app.js"
  end

  test "Link to compiled CSS is present in HTML code", %{conn: conn} do
    conn = get conn, "/"
    assert html_response(conn, 200) =~ "app.css"
  end
end
