FROM chvanikoff/phoenix

COPY . /var/app/main

WORKDIR /var/app/main

ENV MIX_ENV prod

RUN cd assets \
  && npm install \
  && node_modules/.bin/webpack --progress --colors

RUN mix do compile, phx.digest

CMD ["make", "setup_prod", "run_prod"]
