defmodule Main.Web.PageController do
  use Main.Web, :controller

  def index(conn, _params) do
    render conn, "index.html"
  end
end
